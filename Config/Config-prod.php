<?php

namespace App;

/**
 * Application configuration
 *
 * PHP version 7.0
 */
class Config
{

    /**
     * Database host
     * @var string
     */
    const DB_HOST = 'db';

    /**
     * Database port
     * @var string
     */
    const DB_PORT = '3307';

    /**
     * Database name
     * @var string
     */
    const DB_NAME = 'videgrenierenligne-prod'; // Mettez ici le nom de la base de données pour l'environnement de production

    /**
     * Database user
     * @var string
     */
    const DB_USER = 'root';

    /**
     * Database password
     * @var string
     */
    const DB_PASSWORD = 'root';

    /**
     * Show or hide error messages on screen
     * @var boolean
     */
    const SHOW_ERRORS = false; // Réglez sur false pour masquer les messages d'erreur en production
}
