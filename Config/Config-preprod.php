<?php

namespace App;

/**
 * Application configuration
 *
 * PHP version 7.0
 */
class Config
{

    /**
     * Database host
     * @var string
     */
    const DB_HOST = 'db';

    /**
     * Database port
     * @var string
     */
    const DB_PORT = '3308';

    /**
     * Database name
     * @var string
     */
    const DB_NAME = 'videgrenierenligne-preprod';

    /**
     * Database user
     * @var string
     */
    const DB_USER = 'root';

    /**
     * Database password
     * @var string
     */
    const DB_PASSWORD = 'root';
//mdp : 653rag9T
    /**
     * Show or hide error messages on screen
     * @var boolean
     */
    const SHOW_ERRORS = true;
}
