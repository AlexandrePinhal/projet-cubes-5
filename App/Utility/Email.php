<?php

function envoyerEmail($destinataire, $sujet, $message, $expediteur)
{
    // En-têtes de l'e-mail
    $headers = "From: $expediteur" . "\r\n";
    $headers .= "Reply-To: tixagoj870@fitwl.com" . "\r\n";
    $headers .= "Content-Type: text/plain; charset=UTF-8\r\n";

    // Envoi de l'e-mail
    mail($destinataire, $sujet, $message, $headers);
}
