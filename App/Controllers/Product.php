<?php

namespace App\Controllers;

use App\Models\Articles;
use App\Utility\Upload;
use \Core\View;

/**
 * Product controller
 */
class Product extends \Core\Controller
{

    /**
     * Affiche la page d'ajout
     * @return void
     */
    public function indexAction()
    {
        $error = '';

        if (isset($_POST['submit'])) {
            try {
                $f = $_POST;

                // TODO: Validation

                if (is_uploaded_file($_FILES['picture']['tmp_name']) && getimagesize($_FILES['picture']['tmp_name'])) {
                    // Formulaire valide, procéder au traitement

                    $f['user_id'] = $_SESSION['user']['id'];
                    $id = Articles::save($f);

                    if (!empty($_FILES['picture']['name'])) {
                        $pictureName = Upload::uploadFile($_FILES['picture'], $id);
                        Articles::attachPicture($id, $pictureName);
                    }

                    header('Location: /product/' . $id);
                    exit(); // Terminer le script après la redirection
                } else {
                    $error = 'Le formulaire n\'est pas valide. Veuillez ajouter une photo de votre objet.';
                }
            } catch (\Exception $e) {
                var_dump($e);
            }
        }

        View::renderTemplate('Product/Add.html', [
            'error' => $error
        ]);
    }


    /**
     * Affiche la page d'un produit
     * @return void
     */
    public function showAction()
    {
        $id = $this->route_params['id'];

        try {
            Articles::addOneView($id);
            $suggestions = Articles::getSuggest();
            $article = Articles::getOne($id);
        } catch (\Exception $e) {
            var_dump($e);
        }

        View::renderTemplate('Product/Show.html', [
            'article' => $article[0],
            'suggestions' => $suggestions,
            'id' => $id
        ]);
    }

        /**
     * Affiche la page d'un produit
     * @return void
     */
    public function contactAction()
    {
        $id = $this->route_params['id'];

        try {
            Articles::addOneView($id);
            $suggestions = Articles::getSuggest();
            $article = Articles::getOne($id);
        } catch(\Exception $e){
            var_dump($e);
        }

        View::renderTemplate('Product/Contact.html', [
            'article' => $article[0],
            'suggestions' => $suggestions
        ]);
    }
}
