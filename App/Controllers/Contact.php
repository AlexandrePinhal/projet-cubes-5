<?php

require '/vendor/autoload.php';

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

class ContactController
{
    public function traiterFormulaireContact()
    {
        // Vérification des données envoyées via le formulaire
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            // Récupération des données du formulaire
            $nom = $_POST['nom'];
            $expediteur = $_POST['emailFrom'];
            $message = $_POST['message'];
            $emailTo = $_POST['emailTo'];

            // Validation des données (si nécessaire)

            // Envoi de l'e-mail
            $mail = new PHPMailer(true);
            try {
                // Paramètres SMTP Gmail
                $mail->isSMTP();
                $mail->Host = 'smtp.gmail.com';
                $mail->SMTPAuth = true;
                $mail->Username = 'videgrenierenlignecontact@gmail.com';
                $mail->Password = 'videgrenierenligne';
                $mail->SMTPSecure = 'tls';
                $mail->Port = 587;

                // Expéditeur et destinataire
                $mail->setFrom($expediteur);
                $mail->addAddress($emailTo);

                // Contenu de l'e-mail
                $mail->Subject = 'Nouveau message de contact videgrenierenligne';
                $mail->Body = "Nom : $nom\nEmail : $expediteur\nMessage : $message";

                // Envoi de l'e-mail
                $mail->send();

                // Redirection vers une page de confirmation (vue)
                header('Location: confirmation.php');
                exit();
            } catch (Exception $e) {
                // Gestion des erreurs
                echo "Une erreur est survenue lors de l'envoi de l'e-mail : {$mail->ErrorInfo}";
            }
        }
    }
}
