<?php

/**
 * Front controller
 *
 * PHP version 7.0
 */

session_start();

/**
 * Composer
 */
require dirname(__DIR__) . '/vendor/autoload.php';

/**
 * Error and Exception handling
 */
error_reporting(E_ALL);
set_error_handler('Core\Error::errorHandler');
set_exception_handler('Core\Error::exceptionHandler');

/**
 * Routing
 */
$router = new Core\Router();

// Add the routes
$router->add('', ['controller' => 'Home', 'action' => 'index']);
$router->add('login', ['controller' => 'User', 'action' => 'login']);
$router->add('register', ['controller' => 'User', 'action' => 'register']);
$router->add('logout', ['controller' => 'User', 'action' => 'logout', 'private' => true]);
$router->add('account', ['controller' => 'User', 'action' => 'account', 'private' => true]);
$router->add('product', ['controller' => 'Product', 'action' => 'index', 'private' => true]);
$router->add('product/{id:\d+}', ['controller' => 'Product', 'action' => 'show']);
$router->add('contact/{id:\d+}', ['controller' => 'Product', 'action' => 'contact']);
$router->add('{controller}/{action}');
$router->add('cookies', ['controller' => 'Politics', 'action' => 'index']);

/**
 * Gestion des erreurs dans le routing
 */
try {
    $router->dispatch($_SERVER['QUERY_STRING']);
} catch(Exception $e){
    switch($e->getMessage()){
        case 'You must be logged in':
            header('Location: /login');
            break;
        default:
            displayErrorPage(404);
            break;
    }
}

/**
 * Affiche la page d'erreur correspondante au code d'erreur fourni
 *
 * @param int $errorCode Le code d'erreur HTTP
 */
function displayErrorPage($errorCode)
{
    $errorPage = '';
    switch ($errorCode) {
        case 404:
            $errorPage = '404.html';
            break;
        case 500:
            $errorPage = '500.html';
            break;
        default:
            $errorPage = '404.html'; // Par défaut, utiliser la page 404 pour les erreurs inconnues
            break;
    }

    // Afficher la page d'erreur
    $loader = new \Twig\Loader\FilesystemLoader(dirname(__DIR__) . '/App/Views');
    $twig = new \Twig\Environment($loader, [
        'debug' => true,
        'cache' => false, // Désactivez le cache pour le développement
    ]);
    $twig->addExtension(new \Twig\Extension\DebugExtension());
    echo $twig->render($errorPage);
    exit;
}
