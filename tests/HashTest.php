<?php declare(strict_types=1);
use PHPUnit\Framework\TestCase;
use App\Utility\Hash;

final class HashTest extends TestCase
{
    public function testSHA256() : void
    {
      $this->assertEquals(hash("sha256", "test string"), Hash::generate("test string"));
    }

    public function testGenerateSalt() : void
    {
      $this->assertNotEquals(Hash::generateSalt(10), Hash::generateSalt(10));
    }
}
